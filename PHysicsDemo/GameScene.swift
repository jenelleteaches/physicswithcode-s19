//
//  GameScene.swift
//  PHysicsDemo
//
//  Created by Parrot on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    let circle = SKSpriteNode(imageNamed: "circle")
    let square = SKSpriteNode(imageNamed: "square")
    let triangle = SKSpriteNode(imageNamed: "triangle")
    
    override func didMove(to view: SKView) {
        
        
        self.backgroundColor = UIColor.black
        
        // Add borders around the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        
        // 1. Set the intial position of the objects
        circle.position = CGPoint(
            x:self.size.width*0.25,
            y:self.size.height/2)
        square.position = CGPoint(
            x:self.size.width/2,
            y:self.size.height/2)
        triangle.position = CGPoint(
            x:self.size.width*0.75,
            y:self.size.height/2)
        
        
        // 2. Add physics bodies to the sprites
        // -----------------------------
        // when you add a physic body,
        // gravity & dynamic = true by default

        // put a circle hitbox around the circle
        self.circle.physicsBody = SKPhysicsBody(circleOfRadius:50)
        self.circle.physicsBody?.affectedByGravity = true
        self.circle.physicsBody?.isDynamic = true
        
        // add hitbox to square
        self.square.physicsBody = SKPhysicsBody(rectangleOf:self.square.size)
        self.square.physicsBody?.affectedByGravity = false
        self.square.physicsBody?.isDynamic = true
        
        // add hitbox to triangle
        self.triangle.physicsBody = SKPhysicsBody(
            texture: self.triangle.texture!,
            size: self.triangle.size)
        self.triangle.physicsBody?.affectedByGravity = false
        self.triangle.physicsBody?.isDynamic = false
        
        
        // 3. Show the sprites on the screen
        addChild(circle)
        addChild(square)
        addChild(triangle)
    }  // end didMove() funciton
    
    // MARK: Function to create sand
    func spawnSand() {
        // 1. create a sand node
        let sandParticle = SKSpriteNode(imageNamed: "sand")
        
        // 2. set the initial position of the sand
        sandParticle.position.x = self.size.width/2      //  x = w/2
        sandParticle.position.y = self.size.height - 100 // y = h-100
        
        // 3. add physics body to sand
        sandParticle.physicsBody = SKPhysicsBody(circleOfRadius:sandParticle.size.width/2)
        
        // By default, dynamic = true, gravity = true
        // You don't need to change the defaults because
        // we  want the sand to fall down & collide into things

        // 3a. Add a "bounce" to your sand
        sandParticle.physicsBody?.restitution = 1.0
        
        // 4. add the sand to the scene
        addChild(sandParticle)
    }
    
    override func update(_ currentTime: TimeInterval) {
        self.spawnSand()
    }
    
}
